$( document ).ready(function() {


   /*Табы для формы*/

    $('.form__tabs').lightTabs();

     $('.main__project__slider__wrap').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      fade: true,
      asNavFor: '.main__project__slider__nav__wrap'
    });

     $('.main__project__slider__nav__wrap').slick({
      slidesToShow: 5,
      slidesToScroll: 1,
      asNavFor: '.main__project__slider__wrap',
      dots: false,
      focusOnSelect: true,
      responsive: [
        {
          breakpoint: 1000,
          settings: {
            centerMode: true,
            centerPadding: '40px',
            slidesToShow: 3
          }
        },
        {
          breakpoint: 480,
          settings: {
            centerMode: true,
            centerPadding: '40px',
            slidesToShow: 1
          }
        }
      ]
    });

    $('.reviews__slider__wrap').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      infinite: true
    });

    /*Скролл с ссылок к блокам*/

    $('.top__section__links__item, .footer__links__item, .top__stock__block__order__link').click(function (o) {
      event.preventDefault(o);
      $('html, body').animate({
          scrollTop: $($.attr(this, 'href')).offset().top  }, 800);
      if($(this).attr('href') === '#our__projects__block') {
        $('html, body').animate({
          scrollTop: $($.attr(this, 'href')).offset().top +400 }, 800);
      }
      return false;
    });

    /*Скролл к большой SVG фигуре*/
    $('.scroll__form__button').click(function(scrollSVG) {
      event.preventDefault(scrollSVG);
      $('html, body').animate({ scrollTop: $('.svg__transform__block').offset().top -110 }, 500);
    });

    /*Очистка SVG фигуры по клику на кнопку reset*/
    $('.clear__form__button').click(function(clearSvg) {
      $('.svg__transform__block svg>g').not('.svg__show').fadeOut(300);
    });

    /*Появление и закрытие кнопок по подложке и крестику*/

    $('.underlay').click(function(){
      $(this).fadeOut(300);
      $('.pop__up').fadeOut(300);
    });

    $('.close__pop__up').click(function() {
      $(this).parent('.pop__up').fadeOut(300);
      $('.underlay').fadeOut(300);
    });

    $('.result__callback__link, .slider__callback__link, .second__store__link a').click(function(j) {
      event.preventDefault(j);
      $('.underlay').fadeIn(300).delay(300);
      $('.pop__up__callback__form').fadeIn(300).delay(400);
    });

    $('.form__tabs__list__item').click(function() {
      $('.svg__transform__block svg>g').not('.svg__show').fadeOut(300);

      if ($(this).data('page') == 0) {
        $('.heat__form input:checked').each(function () {
          var checkedInput = $(this).val();
          var boilerSvg = $('g[data-boiler="'+checkedInput+'"]').data('boiler');
          var pipeSvg = $('g[data-showpipe="'+checkedInput+'"]').data('showpipe');
          var warmFloorSvg = $('g[data-warmfloor="'+checkedInput+'"]').data('warmfloor');

          if (boilerSvg == checkedInput) {
            $('g[data-boiler="'+boilerSvg+'"]').fadeIn(300);
            $('g[data-boiler]').not('[data-boiler="'+boilerSvg+'"]').fadeOut(300);
          } else if (pipeSvg == checkedInput) {
            $('g[data-showpipe="'+pipeSvg+'"]').fadeIn(300);
            $('g[data-showpipe]').not('[data-showpipe="'+pipeSvg+'"]').fadeOut(300);
            $('.svg__show__pipes').fadeIn(300);
          } else if (warmFloorSvg == checkedInput) {
            $('g[data-warmfloor="'+warmFloorSvg+'"]').fadeIn(300);
            $('g[data-warmfloor]').not('[data-warmfloor="'+warmFloorSvg+'"]').fadeOut(300);
          }
        });
      } else if ($(this).data('page') == 1) {
        $('.water__supply input:checked').each(function() {
          var checkedInput = $(this).val();
          var pipeWaterSvg = $('g[data-showpipewater="'+checkedInput+'"]').data('showpipewater');
          if (pipeWaterSvg == checkedInput) {
            $('g[data-showpipewater="'+pipeWaterSvg+'"]').fadeIn(300);
            $('g[data-showpipewater]').not('[data-showpipewater="'+pipeWaterSvg+'"]').fadeOut(300);
            $('.svg__show__pipes').fadeIn(300);
          }
        });
      } else if ($(this).data('page') == 2) {
        $('.all__in__form input:checked').each(function() {
          var checkedInput = $(this).val();
          var boilerSvg = $('g[data-boiler="'+checkedInput+'"]').data('boiler');
          var pipeSvg = $('g[data-showpipe="'+checkedInput+'"]').data('showpipe');
          var warmFloorSvg = $('g[data-warmfloor="'+checkedInput+'"]').data('warmfloor');
          var pipeWaterSvg = $('g[data-showpipewater="'+checkedInput+'"]').data('showpipewater');

          if (boilerSvg == checkedInput) {
            $('g[data-boiler="'+boilerSvg+'"]').fadeIn(300);
            $('g[data-boiler]').not('[data-boiler="'+boilerSvg+'"]').fadeOut(300);
            console.log('all things good');
          } else if (pipeSvg == checkedInput) {
            $('g[data-showpipe="'+pipeSvg+'"]').fadeIn(300);
            $('g[data-showpipe]').not('[data-showpipe="'+pipeSvg+'"]').fadeOut(300);
          } else if (warmFloorSvg == checkedInput) {
            $('g[data-warmfloor="'+warmFloorSvg+'"]').fadeIn(300);
            $('g[data-warmfloor]').not('[data-warmfloor="'+warmFloorSvg+'"]').fadeOut(300);
          } else if (pipeWaterSvg == checkedInput) {
            $('g[data-showpipewater="'+pipeWaterSvg+'"]').fadeIn(300);
            $('g[data-showpipewater]').not('[data-showpipewater="'+pipeWaterSvg+'"]').fadeOut(300);
          }

          if ($('input[name="wiring__option"], input[name="warm__floor"], input[name="wiring__option__water"]').is(':checked')) {
            $('.svg__show__pipes').fadeIn(300);
          }
        });
      }
    });

    /*Отображение svg элементов по изменению в формах*/

    /*Скрываем трубу у котла*/
    $('g[data-boiler="pelletniy__heat"] .hide__svg__elements').hide();

    $('.form__tabs__content input').on('change', function() {
        
        var checkedInput = $(this).val();
        var boilerSvg = $('g[data-boiler="'+checkedInput+'"]').data('boiler');
        var pipeSvg = $('g[data-showpipe="'+checkedInput+'"]').data('showpipe');
        var warmFloorSvg = $('g[data-warmfloor="'+checkedInput+'"]').data('warmfloor');
        var pipeWaterSvg = $('g[data-showpipewater="'+checkedInput+'"]').data('showpipewater');

        console.log(checkedInput);
        console.log(boilerSvg);

        if (boilerSvg == checkedInput) {
          $('g[data-boiler="'+boilerSvg+'"]').fadeIn(300);
          $('g[data-boiler]').not('[data-boiler="'+boilerSvg+'"]').fadeOut(300);
          console.log('all things good');
        } else if (pipeSvg == checkedInput) {
          $('g[data-showpipe="'+pipeSvg+'"]').fadeIn(300);
          $('g[data-showpipe]').not('[data-showpipe="'+pipeSvg+'"]').fadeOut(300);
        } else if (warmFloorSvg == checkedInput) {
        	$('g[data-warmfloor="'+warmFloorSvg+'"]').fadeIn(300);
          $('g[data-warmfloor]').not('[data-warmfloor="'+warmFloorSvg+'"]').fadeOut(300);
        } else if (pipeWaterSvg == checkedInput) {
        	$('g[data-showpipewater="'+pipeWaterSvg+'"]').fadeIn(300);
          $('g[data-showpipewater]').not('[data-showpipewater="'+pipeWaterSvg+'"]').fadeOut(300);
        }

        if ($('input[name="wiring__option"], input[name="warm__floor"], input[name="wiring__option__water"]').is(':checked')) {
        	$('.svg__show__pipes').fadeIn(300);
        }

        if ($('input#pelletniy').is(':checked') || $('input#pelletniy__all').is(':checked') || $('input#solid__fuel').is(':checked') || $('input#solid__fuel__all').is(':checked')) {
        	$('input[name="heating__method__pelletniy"').prop('disabled' , false);
          $('.labels__for__checkbox').addClass('active');
        } else {
        	$('input[name="heating__method__pelletniy"').prop('disabled' , true);
          $('.labels__for__checkbox').removeClass('active');
          $('input[name="heating__method__pelletniy"').prop('checked' , false);
        }

        if ($('input#well').is(':checked') || $('input#well__all').is(':checked')) {
            $('input[name="well__construction"]').prop('disabled' , false);
            $('input[name="hole__construction"]').prop('checked' , false);
         } else {
            $('input[name="well__construction"]').prop('disabled' , true);
         }

         if ($('input#hole').is(':checked') || $('input#hole__all').is(':checked')) {
            $('input[name="hole__construction"]').prop('disabled' , false);
            $('input[name="well__construction"]').prop('checked' , false);
         } else {
            $('input[name="hole__construction"]').prop('disabled' , true);
         }

        if ($('input#dymohod').is(':checked') || $('input#dymohod__all').is(':checked')) {
        	$('g[data-boiler="pelletniy__heat"] .hide__svg__elements').fadeIn(300);
          $('g[data-boiler="solid__heat"] .hide__svg__elements').fadeIn(300);
        } else {
        	$('g[data-boiler="pelletniy__heat"] .hide__svg__elements').fadeOut(300);
          $('g[data-boiler="solid__heat"] .hide__svg__elements').fadeOut(300);
        }
      });

});